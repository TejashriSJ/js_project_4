function each(items, callBackFunction) {
    if (!Array.isArray(items)) {
        return;
    } else {
        for (let itemIndex = 0; itemIndex < items.length; itemIndex++) {
            let element = items[itemIndex];
            callBackFunction(element, itemIndex, items);
        }
    }

}
module.exports = each;