function filter(items, callBackFunction) {
  if (!Array.isArray(items)) {
    return [];
  } else {
    let result = [];
    for (let index = 0; index < items.length; index++) {
      if (callBackFunction(items[index], index, items) === true) {
        result.push(items[index]);
      }
    }
    return result;
  }
}

module.exports = filter;
