function find(items, callBackFunction) {
  if (!Array.isArray(items)) {
    return [];
  } else {
    for (let index = 0; index < items.length; index++) {
      if (callBackFunction(items[index])) {
        return items[index];
      }
    }
    return undefined;
  }
}

module.exports = find;
