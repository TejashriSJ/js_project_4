function flatten(input, depth, output) {
  // filters out all the holes from the array
  input = input.filter(function (value) {
    if (!value && value !== 0) {
      return false;
    } else {
      return true;
    }
  });

  output = output || []; // if output is undefined assign [] otherwise assign output

  // when depth is not given
  if (!depth && depth !== 0) {
    depth = 1;
  } else if (depth <= 0) {
    return output.concat(input); //when depth is 0 or lesser
  }

  for (let index = 0; index < input.length; index++) {
    let value = input[index];

    if (Array.isArray(value)) {
      if (depth > 1) {
        flatten(value, depth - 1, output);
      } else {
        let indexOfValue = 0;
        while (indexOfValue < value.length) {
          output.push(value[indexOfValue++]);
        }
      }
    } else {
      output.push(value);
    }
  }
  return output;
}

module.exports = flatten;
