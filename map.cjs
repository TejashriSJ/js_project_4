function map(items, callBackFunction) {
    if (!Array.isArray(items)) {
        return [];
    } else {
        let resultArray = [];
        for (let itemIndex = 0; itemIndex < items.length; itemIndex++) {    
            let element = items[itemIndex];
            resultArray.push(callBackFunction(element, itemIndex, items));
        }
        return resultArray;
    }

}
module.exports = map;