function reduce(items, callBackFunction, startingValue) {
  if (!Array.isArray(items)) {
    return [];
  } else {
    if (startingValue === undefined) {
      startingValue = items[0];
      for (let index = 1; index < items.length; index++) {
        startingValue = callBackFunction(
          startingValue,
          items[index],
          index,
          items
        );
      }
    } else {
      for (let index = 0; index < items.length; index++) {
        startingValue = callBackFunction(
          startingValue,
          items[index],
          index,
          items
        );
      }
    }

    return startingValue;
  }
}

module.exports = reduce;
