let each = require('../each.cjs');

const items = [1, 2, 3, 4, 5, 5];

function callBackFunction(element, index, list) {
    console.log(element);
}

each(items, callBackFunction);