let filter = require("../filter.cjs");

const items = [1, 5, 13, "4", 8, 52];

function callBackFunction(element, index, list) {
  if (element < 5) {
    return 1;
  } else {
    return false;
  }
}

console.log(filter(items, callBackFunction));
