let find = require("../find.cjs");

const items = [1, 5, 3, 4, 8, 5];

function callBackFunction(element) {
  if (element > 10) {
    return true;
  } else {
    return false;
  }
}

console.log(find(items, callBackFunction));
