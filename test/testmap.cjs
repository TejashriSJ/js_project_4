let map = require('../map.cjs');

const items = [1, 2, 3, 4, 5, 5];

function callBackFunction(element, index, list) {
    return element * 3;
}

console.log(map(items, callBackFunction));