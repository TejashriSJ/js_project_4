let reduce = require("../reduce.cjs");

const items = [1, 2, 3, 4, 5, 5];
let startingValue;
function callBackFunction(startingValue, element, index, items) {
  return (startingValue += element);
}

console.log(reduce(items, callBackFunction, startingValue));
